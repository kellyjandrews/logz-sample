# Express Lane to Logging with Logz.io

## Obligatory "Why" Statement

If you are a seasoned engineer, or a brand new code school graduate - understanding how your application works is critical to it's success.  With all of the amazing tools available, there is no reason why you shouldn't use some form of logging.  The reasons, as some may well know, are not evident until the time is really critical.  Often we find out our logging isn't what it should be until we _**so desperately need it**_.

1. **Logs are the central source of knowledge** - without them you can't replay history to know what has happened in your app.
2. **Debugging gets a whole lot easier** - There is nothing more difficult than diagnosing something you can't reproduce.
3. **Performance issues are easily spotted** - Logs can help, when analyzed, find patterns that can lead to major performance increase.

## Alright, I Get It - Tell Me How!

There are many ways to get the information you need, but as a JavaScript developer, the simplest route is with a middleware in your Express router. Once you have the middleware in place, you will capture everything you need to know about what is happening with your application and when.

I'm not going to go into a long exhaustive list of what you should be logging right now, but I will show you how to get started, and what path I took to get there quickly.

The method that is outlined below took me 15 minutes from app set up, to first log.  Magic? Not exactly, but close.

### Logz.io

[Logz.io](http://logz.io/) is an online provider of the [Elastic](https://www.elastic.co) (ELK) stack. The open source tools of Elasticsearch, Logstash and Kibana have been around for a while, Elasticsearch having roots back to 2004 when it was called Compass. Each piece helps create a data visualization pipeline to help you diagnose and gain insight into your application.

Setting up all of these tools is no trivial task. It can be done, but the point of this article isn't to learn how to maintain a logging system yourself, it's about getting something going as fast as possible. So, I signed up.

### Building the Application

Building the Express app is nothing complex here. If you want to just cut to the chase, you can [download the repo and built it](https://gitlab.com/kellyjandrews/logz-sample).

For this tutorial, I assume you have some prior knowledge of nodejs, npm and express.  

In an empty folder, set up npm and install the dependencies.

```bash
npm init
npm install express express-winston winston-logzio --save
```

In every express app, you have the ability to use middleware.  Essentially - a function that runs _every time you do something with express_.  [`Winston`](https://github.com/winstonjs/winston) is a logging library to provide transports for logs, or storage systems.  Every time you log something to your console, that's a type of transport.  In our case, we are going to use `winston-logzio` to add a Logz.io transport.

The [`express-winston`](https://github.com/bithavoc/express-winston) library let's us add middleware to express that makes all of this very tidy.

Create an `index.js` file, and add the following require statements -

```javascript
var express = require('express');
var winston = require('winston');
var expressWinston = require('express-winston');
var logzioWinstonTransport = require('winston-logzio');
```

If you notice we have `winston`, but never installed it - the `winston-logzio` package installed that for us, so we are all set.

Next, lets quickly create a simple API to use for testing.

```javascript
const app = express();
const port = process.env.PORT || 3000;

app.get('/', (req, res) => {
  res.status(200).send('Hello World!');
});

app.listen(port, () => {
  console.log('Example app listening on port '+port+'!');
});
```

Simply start up the app using something like `nodemon index.js`. If you don't use `nodemon` already, I highly recommend it to prevent having to stop/restart your application for every change.

### Adding The Middleware

Adding the middleware is quite simple to do. Like with anything, it's not always what you need to do, it's knowing how to do it and get the most out of it.  Just getting setup is fast, making sure you capture everything you will need is real power.

In my Logz.io account, there is a section of log shippers and instruction on how to get them set up.  They have information there about how to set up and use `winston-logzio`, but for this, we just need to grab the options they provide for the token and host.

```javascript
const app = express();
const port = process.env.PORT || 3000;
const loggerOptions = {
    token: 'YOUR_TOKEN_HERE',
    host: 'listener.logz.io'
};
```

Now, it's only a matter of adding the middleware to the application.

```javascript
app.use(expressWinston.logger({
  transports: [new winston.transports.Logzio(loggerOptions)],
  msg: "HTTP {{req.method}} {{req.url}}",
  expressFormat: true
}));

app.get('/', (req, res) => {
  res.status(200).send('Hello World!');
});

app.listen(port, () => {
  console.log('Example app listening on port '+port+'!');
});
```
`expressWinston.logger` is the actual middleware we are using here. There are many different [option available](https://github.com/bithavoc/express-winston#options), but for this we only need a couple of options.

*`tranports`*: This is where we tell Winston to use the Logzio transport. We only have to initialize it using `new winston.transports.Logzio(loggerOptions)` and passing the options to it.  

*'msg'*: more than a flavor enhancement - this let's us customize the log message.  In this instance we are logging the `method` and `url` of each route.

*'expressFormat'*: This uses the default Express/morgan request formatting.

The log to console, looks something like this -

```json
{
  "res": {
    "statusCode": 200
  },
  "req": {
    "url": "/",
    "headers": {
      "host": "localhost:3000",
      "connection": "keep-alive",
      "cache-control": "max-age=0",
      "upgrade-insecure-requests": "1",
      "user-agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36",
      "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
      "accept-encoding": "gzip, deflate, sdch",
      "accept-language": "en-US,en;q=0.8",
      "if-none-match": "W/\"c-7Qdih1MuhjZehB6Sv8UNjA\""
    },
    "method": "GET",
    "httpVersion": "1.1",
    "originalUrl": "/",
    "query": {}
  },
  "responseTime": 1,
  "level": "info",
  "message": "GET / 200 1ms"
}
```

All of the json nodes get passed on to Logz.io, where they then become searchable and actionable. I can use Kibana to visualize the data, create alerts to help see issues faster, and so much more.  Being able to have this kind of insight makes my application better to those who use it, and me a better developer for finding the issues I didn't realize I had.

Feel free to [download the source code](https://gitlab.com/kellyjandrews/logz-sample) and give it a shot to see what you can do. This is just scratching the surface - feel free to shoot me a note on [Twitter](https://www.twitter.com/kellyjandrews) and tell me how you use logging in your application.
