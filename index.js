var express = require('express');
var winston = require('winston');
var expressWinston = require('express-winston');
var logzioWinstonTransport = require('winston-logzio');

const app = express();
const port = process.env.PORT || 3000;
const loggerOptions = {
    token: 'YOUR_TOKEN_HERE',
    host: 'listener.logz.io'
};

app.use(expressWinston.logger({
  transports: [new winston.transports.Logzio(loggerOptions)],
  msg: "HTTP {{req.method}} {{req.url}}",
  expressFormat: true
}));

app.get('/', (req, res) => {
  res.status(200).send('Hello World!');
});

app.listen(port, () => {
  console.log('Example app listening on port '+port+'!');
});
